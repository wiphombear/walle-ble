package cn.songhaiqing.walle.ble.constant;

/**
 * 对外Action
 */
public class WalleAction {
    public final static String GATT_DISCONNECTED = "cn.songhaiqing.walle.ble.ACTION_GATT_DISCONNECTED";
    public final static String GATT_SERVICES_DISCOVERED = "cn.songhaiqing.walle.ble.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String CONNECTED_SUCCESS = "cn.songhaiqing.walle.ble.ACTION_CONNECTED_SUCCESS";
    public static final String CONNECT_FAIL = "cn.songhaiqing.walle.ble.ACTION_CONNECT_FAIL";
    public static final String RECONNECTION = "cn.songhaiqing.walle.ble.ACTION_RECONNECTION";
    public final static String EXECUTED_SUCCESSFULLY = "cn.songhaiqing.walle.ble.ACTION_EXECUTED_SUCCESSFULLY";
    public final static String EXECUTED_FAILED = "cn.songhaiqing.walle.ble.ACTION_EXECUTED_FAILED";
    public final static String DEVICE_RESULT = "cn.songhaiqing.walle.ble.ACTION_DEVICE_RESULT";
    public final static String SCAN_RESULT = "cn.songhaiqing.walle.ble.ACTION_SCAN_RESULT"; // 搜索设备新结果
    public final static String SCAN_TIMEOUT = "cn.songhaiqing.walle.ble.ACTION_SCAN_TIMEOUT"; // 搜索设备超时并结束
    public final static String SERVICES_DISCOVERED_DONE = "cn.songhaiqing.walle.ble.ACTION_SERVICES_DISCOVERED_DONE";
}
